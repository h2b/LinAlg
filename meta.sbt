organization := "de.h2b.scala.lib.math"

organizationName := "private"

organizationHomepage := Some(url("http://h2b.de"))

homepage := Some(url("http://h2b.de"))

startYear := Some(2015)

description := """This is a scala library of algebraic vectors and matrices consisting of 
numerical elements (all standard types are supported, some via implicit numeric 
object) and with arbitrary index ranges.
"""

licenses := Seq("Apache License, Version 2.0" -> url("https://www.apache.org/licenses/LICENSE-2.0"))

pomExtra := Seq(
	<scm>
		<url>scm:git:https://gitlab.com/h2b/LinAlg.git</url>
	</scm>,
	<developers>
		<developer>
			<id>h2b</id>
			<name>Hans-Hermann Bode</name>
			<email>projekte@h2b.de</email>
			<url>http://h2b.de</url>
			<roles>
				<role>Owner</role>
				<role>Architect</role>
				<role>Developer</role>
			</roles>
			<timezone>Europe/Berlin</timezone>
		</developer>
	</developers>
)
