name := "LinAlg"

scalaVersion := "2.12.4"

libraryDependencies ++= Seq(
	"org.scalatest" %% "scalatest" % "3.0.4" % Test,
	"junit" % "junit" % "4.12" % Test,
	"com.novocode" % "junit-interface" % "0.11" % Test,
	"de.h2b.scala.lib" %% "utilib" % "0.4.1" % Test
)