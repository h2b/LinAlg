/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.scala.lib.math.linalg.building

import scala.collection.mutable.{ ArrayBuffer, Builder, Map }
import scala.reflect.ClassTag

import de.h2b.scala.lib.math.linalg.{ SparseVector, Vector }
import de.h2b.scala.lib.math.linalg.Vector.At
import de.h2b.scala.lib.math.linalg.factory.VectorFactory

/**
 * Base trait of vector builders.
 *
 * @param <E> the type of the vector elements
 * @param <V> the type of resulting vector
 * @author h2b
 */
trait VectorBuilder [E, +V] extends Builder[E, V] {

  protected var startIndex = 1;

  protected val elems = ArrayBuffer.empty[E]

  def at (index: Int): this.type = {startIndex = index; this}

  def += (x: E): this.type = {elems += x; this}

  /**
   * Sets the element with the specified index to the given value.
   * Widens the concrete index range if necessary,
   *
   * @param i the index
   * @param x the value
   */
  def update [F <: E : ClassTag] (i: Int, x: F): Unit = {
    val elemsIndex = i-startIndex
    val zero = Vector.scal0[F]
    if (elemsIndex<0) {
      startIndex = i
      ArrayBuffer.fill(-elemsIndex-1)(zero) ++=: elems
      x +=: elems
    } else if (elemsIndex>=elems.length) {
      elems ++= ArrayBuffer.fill(elemsIndex-elems.length)(zero)
      elems += x
    } else {
      elems(elemsIndex) = x
    }
  }

  def clear () = elems.clear()

  def result (): V

}

object VectorBuilder {

	/**
	 * @return a default vector builder
	 */
	def apply [E : ClassTag] (): VectorBuilder[E, Vector[E]] =
	  new DefaultVectorBuilder[E]

	/**
	 * @since 3.0.0
	 * @return a builder for sparse vectors
	 */
	def sparse [E : ClassTag] (): VectorBuilder[E, SparseVector[E]] =
	  new SparseVectorBuilder[E]

}

class DefaultVectorBuilder [E : ClassTag] private[linalg] extends
    VectorBuilder[E, Vector[E]] {

  def result () = VectorFactory(At(startIndex), elems)

}

/**
 * @note It is unfortunate inheriting the `elems` array which is useless
 * here, but we keep the super trait as it was before for compatibility
 * reasons.
 *
 * @since 3.0.0
 * @author h2b
 */
class SparseVectorBuilder [E : ClassTag] private[linalg] extends
    VectorBuilder[E, SparseVector[E]] {

  protected val elemsMap = Map.empty[Int, E]

  private var currentIndex = startIndex

  /**
   * Sets the element to the given value which index is defined by:
   *
   * - the default start index 1 (if nothing of the following applies) or
   *
   * - the next index following the last `update` operation.
   *
   * In all cases, an already existing value at this index will be overridden.
   */
  override def += (x: E): this.type = {
    elemsMap(currentIndex) = x
    currentIndex += 1
    this
  }

  /**
   * Sets the element with the specified index to the given value.
   *
   * In case, there exists already a value at this index, it will be
   * overridden.
   *
   * @param i the index
   * @param x the value
   */
  override def update [F <: E : ClassTag] (i: Int, x: F): Unit = {
    elemsMap(i) = x
    currentIndex = i+1
  }

  override def clear () = elemsMap.clear()

  def result () = VectorFactory(
      elemsMap.toMap map { e ⇒ (e._1 + startIndex - 1, e._2) }
	)

}
