/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.linalg

import de.h2b.scala.lib.math.linalg.building.VectorCanBuildFrom

/**
 * Implementation trait for vectors of type `Vector[E}`.
 *
 * @param <E> type of the vector elements
 * @param <V> type of resulting vector
 * @since 2.0.0
 * @author h2b
 */
trait VectorLike [E, +V <: Vector[E]] extends GenVectorLike[E, V] {

  override def @@ (at: Int): V = {
    val builder = newBuilder at at
    for (elem ← thisCollection) builder += elem
    builder.result()
  }

  override def shorten: V = {
    val scal0 = Vector.scal0[E]
    val coll = thisCollection
    val index = coll.index
    val low = index.find(coll(_)!=scal0).getOrElse(1)
    val high = index.reverse.find(coll(_)!=scal0).getOrElse(0)
    val builder = newBuilder at low
    for (i ← low to high) builder += coll(i)
    builder.result()
  }

  override def widen (index: Index): V = {
    val scal0 = Vector.scal0[E]
    val coll = thisCollection
    val collIndex = coll.index
    val builder = newBuilder at (index.low min collIndex.low)
    for (i ← index.low until collIndex.low) builder += scal0
    for (i ← collIndex.low to collIndex.high) builder += coll(i)
    for (i ← collIndex.high+1 to index.high) builder += scal0
    builder.result()
  }

  override protected def unaryOp (f: E ⇒ E): V = {
    val builder = newBuilder at thisCollection.index.low
    for (elem ← thisCollection) builder += f(elem)
    builder.result()
  }

  override protected def binaryOp [That >: V <: Vector[E]] (f: (E, E) ⇒ E,
      v: That) (implicit bf: VectorCanBuildFrom[this.type, E, That]): That = {
    val u = thisCollection
    val index = u.index union v.index
    val builder = bf() at index.low
    for (i <- index) builder += f(u(i), v(i))
    builder.result()
  }

}
