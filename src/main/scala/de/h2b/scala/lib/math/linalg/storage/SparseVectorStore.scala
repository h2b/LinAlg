/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.linalg.storage

import scala.collection.immutable.ListMap

import de.h2b.scala.lib.math.linalg.{ Index, Vector }

/**
 * Implementation trait for sparse vectors.
 *
 * @param <E> type of the elements
 * @since 3.0.0
 * @author h2b
 */
trait SparseVectorStore [E] extends VectorStore[E] {

  protected val elems: Map[Int, E]

  private val data = ListMap(elems.toSeq.sortBy(_._1): _*)
  protected val keys = data.keys
  protected val values = data.values

  protected val dataHashCode: Int = {
    val prime = 17
    var result = dataHashStart
    for (k ← keys) {
      result = prime*result+k
      result = prime*result+data(k).hashCode()
    }
    result
  }

  val index = if (keys.isEmpty) Index(1, 0) else Index(keys.min, keys.max)

  private val zero = Vector.scal0[E]

  def apply (i: Int): E = data.getOrElse(i, zero)

}
