/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.linalg.factory

import de.h2b.scala.lib.math.linalg.{ SparseVector, Vector, VectorLike }
import de.h2b.scala.lib.math.linalg.storage.SimpleVectorStore

/**
 * An algebraic vector consisting of {@code Double} elements with index range
 * {@code [i0, i0+length-1]}.
 *
 * @author h2b
 */
abstract class DoubleVector protected (protected val i0: Int,
    protected val elemSeq: Seq[Double]) extends ScalarSeq(i0, elemSeq) with
    DoubleOps with VectorLike[Double, Vector[Double]]

/**
 * An algebraic vector consisting of {@code Double} elements constituted by the
 * specified index-value pairs.
 *
 * @since 3.0.0
 * @author h2b
 */
abstract class SparseDoubleVector protected (protected val elemMap:
    Map[Int, Double]) extends ScalarMap(elemMap) with DoubleOps

object DoubleVector {

  def apply (n: Int, elems: Seq[Double]) = new DoubleVector(n, elems) with
      SimpleVectorStore[Double]

}

object SparseDoubleVector {

  def apply (elems: Map[Int, Double]) = new SparseDoubleVector(elems) with
      SparseVector[Double]

}

object DoubleVectorFactory extends VectorFactory[Double] {

  val zero = 0.0
  val one = 1.0

  def create (startIndex: Int, elems: Seq[Double]): Vector[Double] =
    DoubleVector(startIndex, elems)

  def create (elems: Map[Int, Double]): SparseVector[Double] =
    SparseDoubleVector(elems)

}
