/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.linalg

import scala.reflect.ClassTag

import de.h2b.scala.lib.math.linalg.building.{ MatrixBuilder, MatrixCanBuildFrom }
import de.h2b.scala.lib.math.linalg.factory.MatrixFactory
import de.h2b.scala.lib.math.linalg.storage.SparseRowMatrixStore

/**
 * An immutable algebraic matrix with sparse storage of {@code E} elements.
 *
 * @since 3.0.0
 * @author h2b
 */
trait SparseMatrix [E] extends Matrix[E] with
    SparseMatrixLike[E, SparseMatrix[E]] with SparseRowMatrixStore[E] {

  /**
   * Iterates over the sparse row values of this sparse matrix.
   */
  def sparseIterator = sparseRowIterator

  /**
   * Iterates over the sparse row values of this sparse matrix.
   */
	def sparseRowIterator = new Iterator[Vector[E]] {
	  private val valuesIterator = values.iterator
	  def hasNext = valuesIterator.hasNext
	  def next = {
		  if (!hasNext) throw new NoSuchElementException("iterator overflow")
		  valuesIterator.next()
	  }
  }

  /**
   * Iterates over the sparse row indices of this sparse matrix.
   */
	def sparseRowIndexIterator = new Iterator[Int] {
	  private val keysIterator = keys.iterator
	  def hasNext = keysIterator.hasNext
	  def next = {
		  if (!hasNext) throw new NoSuchElementException("iterator overflow")
		  keysIterator.next()
	  }
  }

  /**
   * Iterates over the sparse row indices of this sparse matrix.
   */
	def sparseIndexIterator = sparseRowIndexIterator

	/**
	 * Applies the given function to all sparse row values of this sparse matrix.
	 */
  def sparseForeach [U] (f: (Vector[E]) ⇒ U): Unit =
	  sparseIterator.foreach(f)

  override protected[this] def newBuilder:
      MatrixBuilder[Vector[E], SparseMatrix[E]] =
    SparseMatrix.newBuilder

  override def toString = {
    val pairs = keys map { i ⇒ (i, apply(i)) }
    val strings = pairs map { p ⇒ p._1 + ":" + p._2 }
    strings.mkString("(", ",", ")")
  }
}

object SparseMatrix {

  def newBuilder [E : ClassTag]: MatrixBuilder[Vector[E], SparseMatrix[E]] =
    MatrixBuilder.sparse[E]()

  implicit def canBuildFrom [E : ClassTag]:
      MatrixCanBuildFrom[SparseMatrix[_], Vector[E], SparseMatrix[E]] =
    new MatrixCanBuildFrom[SparseMatrix[_], Vector[E], SparseMatrix[E]] {
	    def apply (): MatrixBuilder[Vector[E], SparseMatrix[E]] = newBuilder[E]
      def apply (from: SparseMatrix[_]): MatrixBuilder[Vector[E], SparseMatrix[E]] =
        newBuilder[E] at from.index.dim1.low
  }

  def fromMap [E : ClassTag] (map: Map[Int, Vector[E]]) = MatrixFactory(map)

  /**
   * @param elems index-value pairs
   * @return a vector with sparse element storage
   */
  def apply [E : ClassTag] (elems: (Int, Vector[E])*) = fromMap(elems.toMap)

}
