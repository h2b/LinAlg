/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.scala.lib.math.linalg.factory

import de.h2b.scala.lib.math.linalg.{ SparseVector, Vector, VectorLike }
import de.h2b.scala.lib.math.linalg.storage.SimpleVectorStore

/**
 * An algebraic vector consisting of {@code Int} elements with index range
 * {@code [i0, i0+length-1]}.
 *
 * @author h2b
 */
abstract class IntVector protected (protected val i0: Int,
    protected val elemSeq: Seq[Int]) extends ScalarSeq(i0, elemSeq) with
    IntOps with VectorLike[Int, Vector[Int]]

/**
 * An algebraic vector consisting of {@code Int} elements constituted by the
 * specified index-value pairs.
 *
 * @since 3.0.0
 * @author h2b
 */
abstract class SparseIntVector protected (protected val elemMap:
    Map[Int, Int]) extends ScalarMap(elemMap) with IntOps

object IntVector {

  def apply (n: Int, elems: Seq[Int]) = new IntVector(n, elems) with
      SimpleVectorStore[Int]

}

object SparseIntVector {

  def apply (elems: Map[Int, Int]) = new SparseIntVector(elems) with
      SparseVector[Int]

}

object IntVectorFactory extends VectorFactory[Int] {

  val zero = 0
  val one = 1

  def create (startIndex: Int, elems: Seq[Int]): Vector[Int] =
    IntVector(startIndex, elems)

  def create (elems: Map[Int, Int]): SparseVector[Int] =
    SparseIntVector(elems)

}
