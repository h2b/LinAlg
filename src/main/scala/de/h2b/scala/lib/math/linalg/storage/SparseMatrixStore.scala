/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.linalg.storage

import scala.collection.immutable.ListMap

import de.h2b.scala.lib.math.linalg.{ Index, Index2, Vector }

/**
 * Implementation trait for matrices that are sparse on rows.
 *
 * @param <E> type of the elements
 * @since 3.0.0
 * @author h2b
 */
trait SparseRowMatrixStore [E] extends MatrixStore[E] {

  protected val rows: Map[Int, Vector[E]]

  private val data = ListMap(rows.toSeq.sortBy(_._1): _*)
  protected val keys = data.keys
  protected val values = data.values

  private val rowIndex =
    if (keys.isEmpty) Index(1, 0) else Index(keys.min, keys.max)

  private val colIndex = {
    var min = Index.Maxdex
    var max = Index.Mindex
    for (v <- values)
      if (v.index.low<min) min = v.index.low
      else if (v.index.high>max) max = v.index.high
    Index(min, max)
  }

  val index = Index2(rowIndex, colIndex)

  private val zerovec = Vector.at[E](colIndex.low)()

  protected val dataHashCode: Int = {
    val prime = 17
    var result = dataHashStart
    for (k ← keys) {
      result = prime*result+k
      result = prime*result+data(k).hashCode()
    }
    result
  }

  def row (i: Int): Vector[E] = data.getOrElse(i, zerovec)

  def col (j: Int): Vector[E] =
    Vector((i: Int) => data.getOrElse(i, zerovec)(j), rowIndex.low, rowIndex.high)

  def apply (i: Int, j: Int): E = row(i)(j)

}
