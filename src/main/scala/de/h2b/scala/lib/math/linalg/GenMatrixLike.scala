/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.linalg

import scala.collection.IterableLike
import scala.reflect.ClassTag

import de.h2b.scala.lib.math.linalg.building.{ MatrixBuilder,
  MatrixCanBuildFrom, VectorCanBuildFrom }

/**
 * A template trait for matrices with operations suitable for all kinds of data
 * storage. For performance reasons, some methods might be overriden by
 * implementation traits in regard to specific storage engines.
 *
 * @param <E> type of the vector elements
 * @param <V> type of resulting vector
 * @since 3.0.0
 * @author h2b
 */
trait GenMatrixLike [E, +M <: Matrix[E]] extends IterableLike[Vector[E], M] {

  override protected[this] def thisCollection: M = this.asInstanceOf[M]
  override protected[this] def toCollection(repr: M): M = repr.asInstanceOf[M]

  override protected[this] def newBuilder: MatrixBuilder[Vector[E], M]

  protected implicit val elemTag: ClassTag[E]

	/**
	 * @param at the lower row-index bound of the new matrix
	 * @return a copy of this matrix with the specified lower row-index bound
	 */
	def atRow (at: Int): M = {
    val builder = newBuilder at at
    for (i ← thisCollection.index.dim1) builder(i) = thisCollection(i)
    builder.result()
	}

	/**
	 * @param at the lower column-index bound of the new matrix
	 * @return a copy of this matrix with the specified lower column-index bound;
	 * all row vectors are shifted to this bound
	 */
	def atCol (at: Int): M = {
    val builder = newBuilder
    for (i ← thisCollection.index.dim1) builder(i) = thisCollection(i) @@ at
    builder.result()
	}

	/**
	 * @param at tuple of the lower row- and column-index bounds of the new matrix
	 * @return a copy of this matrix with the specified lower row- and
	 * column-index bounds; all row vectors are shifted to this bound
	 */
	def @@ (at: (Int, Int)): M = {
	  (thisCollection atRow at._1 atCol at._2).asInstanceOf[M]
	}

  /**
   * @return this matrix with a shortened index range stripped by leading and
   * trailing zero-vector elements after shortening the vector elements
   * itself (i.e., making concrete leading and trailing zeroes virtual in
   * both dimensions)
   *
   * @since 2.1.0
   */
	def shorten: M = {
	  val zerovec = Vector[E]()
	  val coll = thisCollection map { (x: Vector[E]) => x.shorten }
	  val index = coll.index.dim1
    val low = index.find(!coll(_).isZero).getOrElse(1)
    val high = index.reverse.find(!coll(_).isZero).getOrElse(0)
    val builder = newBuilder
    for (i ← low to high) builder(i) = coll(i)
    builder.result()
	}

  /**
   * @param index the requested index ranges in both dimensions (the actual
   * index ranges will be the union of this argument and the existing ones)
   * @return this vector with widened index ranges extended by leading and
   * trailing zero-vector elements according to `index.dim1` and then widening
   * the vector elements itself according to `index.dim2` (i.e., in both
   * dimensions adding concrete leading and trailing zeroes that were virtual
   * before)
   *
   * @since 2.1.0
   */
  def widen (index: Index2): M = {
	  val zerovec = Vector[E]().widen(index.dim2)
    val coll = thisCollection map { (x: Vector[E]) => x.widen(index.dim2) }
    val collIndex = coll.index.dim1
    val builder = newBuilder
    for (i ← index.dim1.low until collIndex.low) builder(i) = zerovec
    for (i ← collIndex.low to collIndex.high) builder(i) = coll(i)
    for (i ← collIndex.high+1 to index.dim1.high) builder(i) = zerovec
    builder.result()
  }

  protected def unaryOp (f: Vector[E] ⇒ Vector[E]): M = {
    val builder = newBuilder
    for (i ← thisCollection.index.dim1) builder(i) = f(thisCollection(i))
    builder.result()
  }

  /**
   * Returns identity. Can also be used to copy '''this'''.
   *
   * @return +'''this'''
   */
  def unary_+ (): M = unaryOp((x: Vector[E]) ⇒ x)

  /**
   * Returns negative complement of '''this'''.
   *
   * @return -'''this'''
   */
  def unary_- (): M = unaryOp((x: Vector[E]) ⇒ -x)

  /**
   * Scales this matrix by `s`.
   *
   * @param s
   * @return '''this'''*`s`
   */
  def * (s: E): M = unaryOp((x: Vector[E]) ⇒ x*s)

  /**
   * Returns the transpose of this matrix..
   *
   * @return '''this'''^T^
   */
  def transposed (): M = {
    val builder = newBuilder
    for (i <- thisCollection.index.dim2) builder(i) = thisCollection.col(i)
    builder.result()
  }

  protected def binaryOp [That >: M <: Matrix[E]]
      (f: (Vector[E], Vector[E]) ⇒ Vector[E], b: That)
      (implicit bf: MatrixCanBuildFrom[this.type, Vector[E], That]): That = {
    val a = thisCollection
    val index1 = a.index.dim1 union b.index.dim1
    val builder = bf()
    for (i <- index1) builder(i) = f(a(i), b(i))
    builder.result()
  }

  /**
   * Returns the sum of '''this''' and `b`.
   *
   * @param b the other matrix to be added
   * @return '''this'''+`b`
   */
  def + [That >: M <: Matrix[E]] (b: That)
      (implicit vbf: VectorCanBuildFrom[Vector[E], E, Vector[E]],
          mbf: MatrixCanBuildFrom[this.type, Vector[E], That]): That =
    binaryOp((x: Vector[E], y: Vector[E]) ⇒ x+y, b)

  /**
   * Returns the difference of '''this''' and `b`.
   *
   * @param b the other matrix to be subtracted
   * @return  '''this'''-`b`
   */
  def - [That >: M <: Matrix[E]] (b: That)
      (implicit vbf: VectorCanBuildFrom[Vector[E], E, Vector[E]],
          mbf: MatrixCanBuildFrom[this.type, Vector[E], That]): That =
    binaryOp((x: Vector[E], y: Vector[E]) ⇒ x-y, b)

  /**
   * Returns the product of '''this''' and `b`.
   *
   * @param b the other matrix to be multiplied
   * @return '''this'''*`b`
   */
  def * [That >: M <: Matrix[E]] (b: That)
      (implicit vbf: VectorCanBuildFrom[Vector[E], E, Vector[E]],
          mbf: MatrixCanBuildFrom[this.type, Vector[E], That]): That = {
    val a = thisCollection
    val mbuilder = mbf()
    for (i <- a.index.dim1) {
      val ai = a.row(i)
      val vbuilder = vbf()
      for (j <- b.index.dim2) vbuilder(j) = ai*b.col(j)
      mbuilder(i) = vbuilder.result()
    }
    mbuilder.result()
  }

  /**
   * Returns the product of '''this''' and `v`.
   *
   * @param v the vector to be multiplied
   * @return '''this'''*`v`
   */
  def * [That <: Vector[E]] (v: That)
      (implicit vbf: VectorCanBuildFrom[That, E, That]): That = {
    val a = thisCollection
    val vbuilder = vbf()
    for (i <- a.index.dim1) vbuilder(i) = a(i)*v
    vbuilder.result()
  }

}
