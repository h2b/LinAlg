/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.linalg

import de.h2b.scala.lib.math.linalg.building.{ MatrixCanBuildFrom,
    VectorCanBuildFrom }

/**
 * Implementation trait for matrices of type `SparseMatrix[E]`.
 *
 * @param <E> type of the vector elements
 * @param <M> type of resulting matrix
 * @since 3.0.0
 * @author h2b
 */
trait SparseMatrixLike [E, +M <: SparseMatrix[E]] extends GenMatrixLike[E, M] {

	override def atRow (at: Int): M = {
    val builder = newBuilder
    val origin = at - thisCollection.index.dim1.low
    for (i ← thisCollection.sparseRowIndexIterator)
      builder(origin + i) = thisCollection(i)
    builder.result()
	}

	override def shorten: M =
	  @@((thisCollection.index.dim1.low, thisCollection.index.dim2.low))

  override def widen (index: Index2): M =
	  @@((thisCollection.index.dim1.low, thisCollection.index.dim2.low))

  override protected def unaryOp (f: Vector[E] ⇒ Vector[E]): M = {
    val builder = newBuilder
    for (i ← thisCollection.sparseRowIndexIterator)
      builder(i) = f(thisCollection(i))
    builder.result()
  }

  override def * [That >: M <: Matrix[E]] (b: That)
      (implicit vbf: VectorCanBuildFrom[Vector[E], E, Vector[E]],
          mbf: MatrixCanBuildFrom[this.type, Vector[E], That]): That = {
    val a = thisCollection
    val mbuilder = mbf()
    for (i <- a.sparseRowIndexIterator) {
      val ai = a.row(i)
      val vbuilder = vbf()
      for (j <- b.index.dim2) vbuilder(j) = ai*b.col(j)
      mbuilder(i) = vbuilder.result()
    }
    mbuilder.result()
  }

  override def * [That <: Vector[E]] (v: That)
      (implicit vbf: VectorCanBuildFrom[That, E, That]): That = {
    val a = thisCollection
    val vbuilder = vbf()
    for (i <- a.sparseRowIndexIterator) vbuilder(i) = a(i)*v
    vbuilder.result()
  }

}
