/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.linalg.storage

import scala.reflect.ClassTag

import de.h2b.scala.lib.math.linalg.{ Index, Vector }

/**
 * Definition trait for storage of vector elements.
 *
 * Implementations of this trait need to provide concrete methods with
 * signatures
 * {{{
 *   def index: Index
 *   def apply (i: Int): E
 *   protected def dataHashCode: Int
 * }}}
 * The `dataHashCode` method must be consistent over all implementations to
 * ensure equality between different implementations of the same data.
 *
 * @param <E> type of the elements
 * @since 2.0.0
 * @author h2b
 */
trait VectorStore [E] {

  protected implicit val elemTag: ClassTag[E]

  protected final val dataHashStart = 1

	def index: Index

  /**
   * @param i
   * @return the element with index {@code i}
   */
  def apply (i: Int): E

  protected def dataHashCode: Int

  override def hashCode = {
	  var result = 1
    val d = dataHashCode
    if (d!=dataHashStart) {
    	val prime = 31
			result = prime*result+index.low
			result = prime*result+index.high
			result = prime*result+d
    }
    result
  }

}

/**
 * Implementation trait that uses `scala.collection.immutable.Vector` as storage.
 *
 * @param <E> type of the elements
 * @since 2.0.0
 * @author h2b
 */
trait SimpleVectorStore [E] extends VectorStore[E] {

  protected val startIndex: Int
  protected val elems: Seq[E]

  private val data = scala.collection.immutable.Vector(elems: _*)

  val index = Index(startIndex, startIndex+data.length -1)

  private val zero = Vector.scal0[E]

  protected val dataHashCode: Int = {
    val prime = 17
    var result = dataHashStart
    for (i ← index if apply(i)!=zero) {
      result = prime*result+i
      result = prime*result+apply(i).hashCode()
    }
    result
  }

  def apply (i: Int): E = {
		if (index.contains(i)) data(i-index.low) else zero
  }

}
