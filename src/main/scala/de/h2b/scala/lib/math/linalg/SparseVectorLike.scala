/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.linalg

/**
 * Implementation trait for vectors of type `SparseVector[E]`.
 *
 * @param <E> type of the vector elements
 * @param <V> type of resulting vector
 * @since 3.0.0
 * @author h2b
 */
trait SparseVectorLike [E, +V <: SparseVector[E]] extends GenVectorLike[E, V] {

  override def @@ (at: Int): V = {
    val builder = newBuilder
    val origin = at - thisCollection.index.low
    for (i ← thisCollection.sparseIndexIterator)
      builder(origin + i) = thisCollection(i)
    builder.result()
  }

  override def shorten: V = @@(thisCollection.index.low)

  override def widen (index: Index): V = @@(thisCollection.index.low)

  override protected def unaryOp (f: E ⇒ E): V = {
    val builder = newBuilder
    for (i ← thisCollection.sparseIndexIterator)
      builder(i) = f(thisCollection(i))
    builder.result()
  }

}
