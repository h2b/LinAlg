/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.linalg

import scala.reflect.ClassTag

import de.h2b.scala.lib.math.linalg.building.{ VectorBuilder,
  VectorCanBuildFrom }
import de.h2b.scala.lib.math.linalg.factory.VectorFactory
import de.h2b.scala.lib.math.linalg.storage.SparseVectorStore

/**
 * An immutable algebraic vector with sparse storage of {@code E} elements.
 *
 * @since 3.0.0
 * @author h2b
 */
trait SparseVector [E] extends Vector[E] with
    SparseVectorLike[E, SparseVector[E]] with SparseVectorStore[E] {

  override def * (v: Vector[E]): E = {
    var s = Vector.scal0
    for (i <- this.keys) {
      val t = op.times(this(i), v(i))
      s = op.plus(s, t)
    }
    s
  }

  /**
   * Iterates over the sparse values of this sparse vector.
   */
	def sparseIterator = new Iterator[E] {
	  private val valuesIterator = values.iterator
	  def hasNext = valuesIterator.hasNext
	  def next = {
		  if (!hasNext) throw new NoSuchElementException("iterator overflow")
		  valuesIterator.next()
	  }
  }

  /**
   * Iterates over the sparse indices of this sparse vector.
   */
	def sparseIndexIterator = new Iterator[Int] {
	  private val keysIterator = keys.iterator
	  def hasNext = keysIterator.hasNext
	  def next = {
		  if (!hasNext) throw new NoSuchElementException("iterator overflow")
		  keysIterator.next()
	  }
  }

	/**
	 * Applies the given function to all sparse values of this sparse vector.
	 */
	def sparseForeach [U] (f: (E) ⇒ U): Unit = sparseIterator.foreach(f)

  override protected[this] def newBuilder: VectorBuilder[E, SparseVector[E]] =
    SparseVector.newBuilder

  override def toString = {
    val pairs = keys map { i ⇒ (i, apply(i)) }
    val strings = pairs map { p ⇒ p._1 + ":" + p._2 }
    strings.mkString("(", ",", ")")
  }

}

object SparseVector {

  def newBuilder [E : ClassTag]: VectorBuilder[E, SparseVector[E]] =
    VectorBuilder.sparse[E]()

  implicit def canBuildFrom [E : ClassTag]:
      VectorCanBuildFrom[SparseVector[_], E, SparseVector[E]] =
    new VectorCanBuildFrom[SparseVector[_], E, SparseVector[E]] {
	    def apply (): VectorBuilder[E, SparseVector[E]] = newBuilder[E]
      def apply (from: SparseVector[_]): VectorBuilder[E, SparseVector[E]] =
        newBuilder[E] at from.index.low
  }

  def fromMap [E : ClassTag] (map: Map[Int, E]) = VectorFactory(map)

  /**
   * @param elems index-value pairs
   * @return a vector with sparse element storage
   */
  def apply [E : ClassTag] (elems: (Int, E)*) = fromMap(elems.toMap)

}
