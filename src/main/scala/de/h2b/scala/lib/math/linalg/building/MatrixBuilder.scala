/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.scala.lib.math.linalg.building

import scala.collection.mutable.{ ArrayBuffer, Builder, Map }
import scala.reflect.ClassTag

import de.h2b.scala.lib.math.linalg.{ Matrix, SparseMatrix, Vector }
import de.h2b.scala.lib.math.linalg.factory.MatrixFactory

/**
 * Base trait of matrix builders.
 *
 * @param <V> the type of the row or column vectors
 * @param <M> the type of resulting matrix
 * @author h2b
 */
trait MatrixBuilder [V, +M] extends Builder[V, M] {

  protected var rowStart = 1;

  protected val elems = ArrayBuffer.empty[V]

  /**
   * Used to fill empty spots when widening the index range.
   */
  protected val zerovec: V

  def at (row: Int): this.type = { rowStart = row; this }

  def += (x: V): this.type = { elems += x; this }

  def clear () = elems.clear()

  def result (): M

  /**
   * Sets the element with the specified index to the given value.
   * Widens the concrete index range if necessary,
   *
   * @param i the index
   * @param x the value
   */
  def update (i: Int, x: V): Unit = {
    val elemsRow = i-rowStart
    if (elemsRow<0) {
      rowStart = i
      ArrayBuffer.fill(-elemsRow-1)(zerovec) ++=: elems
      x +=: elems
    } else if (elemsRow>=elems.length) {
      elems ++= ArrayBuffer.fill(elemsRow-elems.length)(zerovec)
      elems += x
    } else {
      elems(elemsRow) = x
    }
  }

}

object MatrixBuilder {

	/**
	 * @return a default matrix builder
	 */
	def apply [E : ClassTag] (): MatrixBuilder[Vector[E], Matrix[E]] =
	  new DefaultMatrixBuilder[E]

	/**
	 * @since 3.0.0
	 * @return a builder for sparse matrices
	 */
	def sparse [E : ClassTag] (): MatrixBuilder[Vector[E], SparseMatrix[E]] =
	  new SparseMatrixBuilder[E]

}

class DefaultMatrixBuilder [E : ClassTag] private[linalg] extends
    MatrixBuilder[Vector[E], Matrix[E]] {

  protected val zerovec = Vector[E]()

  def result () = MatrixFactory(rowStart, elems)

}

/**
 * @note It is unfortunate inheriting the `elems` array which is useless
 * here, but we keep the super trait as it was before for compatibility
 * reasons.
 *
 * @since 3.0.0
 * @author h2b
 */
class SparseMatrixBuilder [E : ClassTag] private[linalg] extends
    MatrixBuilder[Vector[E], SparseMatrix[E]] {

  protected val elemsMap = Map.empty[Int, Vector[E]]

  private var currentRow = rowStart

  protected val zerovec = Vector[E]()

  /**
   * Sets the element to the given value which index is defined by:
   *
   * - the default start index 1 (if nothing of the following applies) or
   *
   * - the next index following the last `update` operation.
   *
   * In all cases, an already existing value at this index will be overridden.
   */
  override def += (x: Vector[E]): this.type = {
    elemsMap(currentRow) = x
    currentRow+= 1
    this
  }

  /**
   * Sets the element with the specified index to the given value.
   *
   * In case there exists already a value at this index, it will be
   * overridden.
   *
   * @param i the index
   * @param x the value
   */
  override def update (i: Int, x: Vector[E]): Unit = {
    elemsMap(i) = x
    currentRow = i+1
  }

  override def clear () = elemsMap.clear()

  def result () = MatrixFactory(
      elemsMap.toMap map { e ⇒ (e._1 + rowStart - 1, e._2) }
  )

}
