/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.linalg

import de.h2b.scala.lib.math.linalg.building.{ MatrixCanBuildFrom,
  VectorCanBuildFrom }

/**
 * Implementation trait for matrices of type `Matrix[E}`.
 *
 * @param <E> type of the matrix elements
 * @param <M> type of resulting matrix
 * @since 2.0.0
 * @author h2b
 */
trait MatrixLike [E, +M <: Matrix[E]] extends GenMatrixLike[E, M] {

	override def atRow (at: Int): M = {
    val builder = newBuilder at at
    for (elem ← thisCollection) builder += elem
    builder.result()
	}

	override def atCol (at: Int): M = {
    val builder = newBuilder at thisCollection.index.dim1.low
    for (elem ← thisCollection) builder += elem @@ at
    builder.result()
	}

	override def shorten: M = {
	  val zerovec = Vector[E]()
	  val coll = thisCollection map { (x: Vector[E]) => x.shorten }
	  val index = coll.index.dim1
    val low = index.find(!coll(_).isZero).getOrElse(1)
    val high = index.reverse.find(!coll(_).isZero).getOrElse(0)
    val builder = newBuilder at low
    for (i ← low to high) builder += coll(i)
    builder.result()
	}

  override def widen (index: Index2): M = {
	  val zerovec = Vector[E]().widen(index.dim2)
    val coll = thisCollection map { (x: Vector[E]) => x.widen(index.dim2) }
    val collIndex = coll.index.dim1
    val builder = newBuilder at (index.dim1.low min collIndex.low)
    for (i ← index.dim1.low until collIndex.low) builder += zerovec
    for (i ← collIndex.low to collIndex.high) builder += coll(i)
    for (i ← collIndex.high+1 to index.dim1.high) builder += zerovec
    builder.result()
  }

  override protected def unaryOp (f: Vector[E] ⇒ Vector[E]): M = {
    val builder = newBuilder at thisCollection.index.dim1.low
    for (elem ← thisCollection) builder += f(elem)
    builder.result()
  }

  override def transposed (): M = {
    val builder = newBuilder at thisCollection.index.dim2.low
    for (i <- thisCollection.index.dim2) builder += thisCollection.col(i)
    builder.result()
  }

  override protected def binaryOp [That >: M <: Matrix[E]]
      (f: (Vector[E], Vector[E]) ⇒ Vector[E], b: That)
      (implicit bf: MatrixCanBuildFrom[this.type, Vector[E], That]): That = {
    val a = thisCollection
    val index1 = a.index.dim1 union b.index.dim1
    val builder = bf() at index1.low
    for (i <- index1) builder += f(a(i), b(i))
    builder.result()
  }

  override def * [That >: M <: Matrix[E]] (b: That)
      (implicit vbf: VectorCanBuildFrom[Vector[E], E, Vector[E]],
          mbf: MatrixCanBuildFrom[this.type, Vector[E], That]): That = {
    val a = thisCollection
    val mbuilder = mbf() at a.index.dim1.low
    for (i <- a.index.dim1) {
      val ai = a.row(i)
      val vbuilder = vbf() at b.index.dim2.low
      for (j <- b.index.dim2) vbuilder += ai*b.col(j)
      mbuilder += vbuilder.result()
    }
    mbuilder.result()
  }

  override def * [That <: Vector[E]] (v: That)
      (implicit vbf: VectorCanBuildFrom[That, E, That]): That = {
    val a = thisCollection
    val vbuilder = vbf() at a.index.dim1.low
    for (row  <- a.rowIterator) vbuilder += row*v
    vbuilder.result
  }

}
