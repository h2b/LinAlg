/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math

import scala.reflect.ClassTag

import de.h2b.scala.lib.math.linalg.building.VectorBuilder

/**
LinAlg provides data types and operations for algebraic vectors and matrices.

Vector and Matrix elements can be of arbitrary type, provided that a factory
for that element type is available. Currently, factories exist for `Double`,
`Float`, `Long`, `Int`, `Short`, `Byte` and `Char`.

Vector and matrix (row and column) indices can be any integer (to state more
precisely, an index must be in the interval `[Mindex, Maxdex]`, as defined in
the `Index` object). But only elements corresponding to a subset of that whole
integer range actually are stored (the ''concrete'' elements), while all other
elements by definition are zero (''virtual'' elements).

This concept (which I got to value a long time ago with the ALGOL68 library
prelude TORRIX [1]) does not only provide a natural approach to operations
with vectors and matrixes of different index ranges, but also prevents from
index-out-of-bounds exceptions.

Concrete index ranges (those indices that correspond to concrete elements)
implicitly or explicitly are assigned when a vector or matrix is created. By
default, it starts at 1 and extends to the number of elements specified, but
this is customizable.

`Vector`s and `Matrix`es both are immutable by design, so there exist no
`update` methods or the like (you ''cannot'' do `v(i) = something`). There are,
however, builder classes that allow you to build `Vector`s and `Matrix`es
element by element.

Currently, operations on vectors and matrices require identical element
types. For instance, you can add a `Vector[Int]` to another `Vector[Int]`,
but you ''cannot'' add a `Vector[Int]` to another `Vector[Double]`.

 * @author h2b
 * @see [1] S. G. van der Meulen, P. Kühling, "Programmieren in ALGOL68",
 *          Bd. II (Berlin, New York: de Gruyter), 149-188 (1977)
 */
package object linalg {

	/**
	 * Defines vector and matrix operations on the scalar `s` as left operand.
	 *
	 * @param <E> the vector or matrix element's type
	 * @param <F> the scalar's type
	 */
	implicit class ScalarOps [E : ClassTag] (private val s: E) {

	  /**
	   * Scales the specified vector by `s`.
	   *
	   * @param v the vector
	   * @return `s`*`v`
	   */
    def * (v: Vector[E]): Vector[E] = v * s

	  /**
	   * Scales the specified matrix by `s`.
	   *
	   * @param a the matrix
	   * @return `s`*`v`
	   */
    def * (a: Matrix[E]): Matrix[E] = a * s

  }

	/**
	 * Defines matrix operations on the vector `v` as left operand.
	 *
	 * @param <E> the matrix element's type
	 * @param <F> the vectors's element type
	 */
	implicit class VectorOps [E : ClassTag] (private val v: Vector[E]) {

		/**
		 * Returns the product of `v` and the matrix.
		 *
	   * @note This should be named `*`, but there is an ambiguity with
	   * `ScalarOps`. Until this problem is solved, use `**`.
	   *
		 * @param a the matrix to be multiplied
		 * @return `v`*`a`
		 */
    def ** (a: Matrix[E]): Vector[E] = {
  		val vbuilder = VectorBuilder[E]() at a.index.dim2.low
			for (col  <- a.colIterator) vbuilder += v*col
			vbuilder.result()
    }

  }

}
