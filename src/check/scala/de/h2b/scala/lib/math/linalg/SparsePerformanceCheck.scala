/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.linalg

import de.h2b.scala.lib.math.linalg.building.{ MatrixBuilder, VectorBuilder }
import de.h2b.scala.lib.math.stat.Uniform

/**
 * Executes performance checks of various LinAlg sparse operations.
 *
 * @author h2b
 */
object SparsePerformanceCheck extends
    PerformanceCheckTasks[SparseVector[Double], SparseMatrix[Double]] {

  protected val N = 1000
  protected val M = 100
  protected val repeats = 500

  protected val tasks = Seq(
      vectorCreate,
      vectorUnaryPlus, vectorUnaryMinus,
      vectorPlus, vectorMinus,
      vectorScalarProduct, vectorTimesScalar, vectorNorm,
      matrixCreate,
      matrixUnaryPlus, matrixUnaryMinus,
      matrixPlus, matrixMinus,
      matrixTimesMatrix, matrixTimesVector, matrixTimesScalar,
      matrixTransposed, matrixRowSum, matrixColSum
		)

  private def sparsity = 0.9

  private lazy val uniform = Uniform() //lazy to get initialized in task objects

  def randomVector (n: Int) = {
    val builder = VectorBuilder.sparse[Double]
    for (i ← 1 to n) if (uniform.next() > sparsity) builder(i) = uniform.next()
    builder.result()
  }

  def randomMatrix (m: Int, n: Int) = {
    val builder = MatrixBuilder.sparse[Double]
    for (i ← 1 to m) if (uniform.next() > sparsity) builder(i) = randomVector(n)
    builder.result()
  }

  def randomScalar () = uniform.next()

  override def main (args: Array[String]): Unit = super.main(args)

}