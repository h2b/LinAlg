/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.linalg

import java.util.Date

import de.h2b.scala.lib.phys.Time
import de.h2b.scala.lib.util.PerformanceMeter
import de.h2b.scala.lib.util.PerformanceMeter.Task

/**
 * Provides tasks for performance checks of various LinAlg operations.
 *
 * @param <V> the vector type
 * @param <M> the matrix type
 * @author h2b
 */
trait PerformanceCheckTasks [V <: Vector[Double], M <: Matrix[Double]] {

  protected def N: Int
  protected def M: Int
  protected def repeats: Int

  protected def tasks: Seq[Task]

  def randomVector (n: Int): V

  def randomMatrix (m: Int, n: Int): M

  def randomScalar (): Double

  def main (args: Array[String]): Unit = {
	  println(s"""LinAlg -- Performance checks
               |
               |Date: ${new Date()}
  	           |
               |N = $N
               |M = $M
               |Repeats = $repeats
             """.stripMargin)
	  val totals = PerformanceMeter.measurements(tasks, repeats)
	  println("\n                task \t         time/ns\n")
	  for (total ← totals.toList.sortBy(_._1.toString))
	    printf("%20s \t %15.3f\n", total._1, total._2/repeats.toDouble)
	  val sum = totals.values.sum
	  printf("\n%20s \t %15d (%s)\n", "sum", sum, Time(sum/1000000).toString())
  }

  object vectorCreate extends Task {
    def perform () = randomVector(N)
  }

  object vectorUnaryPlus extends Task {
    val v = randomVector(N)
    def perform () = +v
  }

  object vectorUnaryMinus extends Task {
    val v = randomVector(N)
    def perform () = -v
  }

  object vectorPlus extends Task {
    val u = randomVector(N)
    val v = randomVector(N)
    def perform () = u+v
  }

  object vectorMinus extends Task {
    val u = randomVector(N)
    val v = randomVector(N)
    def perform () = u-v
  }

  object vectorScalarProduct extends Task {
    val u = randomVector(N)
    val v = randomVector(N)
    def perform () = u*v
  }

  object vectorTimesScalar extends Task {
    val v = randomVector(N)
    val s = randomScalar()
    def perform () = v*s
  }

  object vectorNorm extends Task {
    val v = randomVector(N)
    def perform () = v.norm
  }

  object matrixCreate extends Task {
	  def perform () = randomMatrix(M, N)
  }

  object matrixUnaryPlus extends Task {
    val a = randomMatrix(M, N)
    def perform () = +a
  }

  object matrixUnaryMinus extends Task {
    val a = randomMatrix(M, N)
    def perform () = -a
  }

  object matrixPlus extends Task {
    val a = randomMatrix(M, N)
    val b = randomMatrix(M, N)
    def perform () = a+b
  }

  object matrixMinus extends Task {
    val a = randomMatrix(M, N)
    val b = randomMatrix(M, N)
    def perform () = a-b
  }

  object matrixTimesMatrix extends Task {
    val a = randomMatrix(M, N)
    val b = randomMatrix(M, N)
    def perform () = a*b
  }

  object matrixTimesVector extends Task {
    val a = randomMatrix(M, N)
    val v = randomVector(N).asInstanceOf[Vector[Double]] //FIXME: casting
    def perform () = a*v
  }

  object matrixTimesScalar extends Task {
    val a = randomMatrix(M, N)
    val s = randomScalar()
    def perform () = a*s
  }

  object matrixTransposed extends Task {
    val a = randomMatrix(M, N)
    def perform () = a.transposed()
  }

  object matrixRowSum extends Task {
    val a = randomMatrix(M, N)
    def perform () = a.rowSum()
  }

  object matrixColSum extends Task {
    val a = randomMatrix(M, N)
    def perform () = a.colSum()
  }

}