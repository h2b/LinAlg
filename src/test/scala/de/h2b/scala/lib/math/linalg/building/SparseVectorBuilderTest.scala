/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.linalg.building

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import de.h2b.scala.lib.math.linalg.Vector

@RunWith(classOf[JUnitRunner])
class SparseVectorBuilderTest extends FunSuite {

  test("sequence") {
	  val expected = Vector.sparse(1→1,2→2,10→10,11→ 11,20→20,21→21,30→30)
    val builder = VectorBuilder.sparse[Int]()
    builder += 1
    builder += 2
    builder(10) = 10
    builder += 11
    builder(20) = 20
    builder += 21
    builder(30) = 30
    assertResult(expected)(builder.result())
  }

  test("sequence at") {
	  val expected = Vector.sparse(11 → 1, 12 → 2, 13 → 3)
    val builder = VectorBuilder.sparse[Int]() at 11
    builder += 1
    builder += 2
    builder += 3
    assertResult(expected)(builder.result())
  }

  test("update") {
	  val expected = Vector.sparse(-10 → -1, 0 → 0, 10 → 1)
    val builder = VectorBuilder.sparse[Int]()
    builder(-10) = -1
    builder(+10) = +1
    val result = builder.result()
    assertResult(expected)(result)
  }

}
