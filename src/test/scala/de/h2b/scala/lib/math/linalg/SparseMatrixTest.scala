/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.linalg

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class SparseMatrixTest extends FunSuite {

  private val sa = Matrix(-1 → Vector(-11,-12,-13), +1 → Vector(11,12,13))
  private val sb = Matrix(1 → Vector(11,12,13), 2 → Vector(21,22,23))
  private val a = Matrix(Vector(-11,-12,-13), Vector[Int](), Vector(11,12,13)).atRow(-1)
  private val b = Matrix(Vector(11,12,13), Vector(21,22,23))

  test("height and width") {
    assertResult(3)(sa.height)
    assertResult(3)(sa.width)
    assertResult(2)(sb.height)
    assertResult(3)(sb.width)
  }

  test("isSquare") {
    assert(sa.isSquare)
    assert(!sb.isSquare)
  }

  test("scaling") {
    assertResult(a*(-2))(-2*sa)
    assertResult(a*(-2))(sa*(-2))
  }

  test("unary operations") {
	  assertResult(a)(+sa)
	  assertResult(-a)(-sa)
  }

  test("binary plus operation") {
    assertResult(a+b)(sa+sb)
    assertResult(a+b)(sa+b)
    assertResult(a+b)(b+sa)
  }

  test("binary minus operation") {
    assertResult(a-b)(sa-sb)
    assertResult(b-a)(sb-sa)
    assertResult(a-b)(sa-b)
    assertResult(b-a)(b-sa)
  }

	test("matrix * matrix") {
	  assertResult(a*b)(a*sb)
	  assertResult(a*b)(sa*b)
	  assertResult(a*b)(sa*sb)
	}

	test("matrix * vector") {
	  val u = Vector(1,2)
	  val su = Vector(1→1,2→2)
		assertResult(a*u)(sa*u)
		assertResult(a*u)(sa*su)
		assertResult(a*u)(a*su)
	}

	test("vector * matrix") {
	  val u = Vector(1,2)
	  val su = Vector(1→1,2→2)
		assertResult(u**a)(u**sa)
		assertResult(u**a)(su**sa)
	}

	test("transposed") {
    assertResult(a.transposed())(sa.transposed())
    assertResult(b.transposed())(sb.transposed())
	}

	test("row sum and column sum") {
	  assertResult(a.rowSum())(sa.rowSum())
	  assertResult(a.colSum())(sa.colSum())
	}

  test("sparse foreach") {
    val as = Matrix(Vector(-11,-12,-13), Vector(11,12,13))
    var i = 1
    sa.sparseForeach(x ⇒ { assertResult(as(i))(x); i +=1 })
    assertResult(as.index.dim1.high+1)(i)
  }

  test("sparse index iterator") {
    assertResult(Seq(-1,1))(sa.sparseIndexIterator.toSeq)
    assertResult(Seq(1,2))(sb.sparseIndexIterator.toSeq)
  }

	test("sparse iterator") {
    val as = Matrix(Vector(-11,-12,-13), Vector(11,12,13))
	  var i = 1
	  for (row <- sa.sparseIterator) { assertResult(as(i))(row); i += 1 }
    assertResult(as.index.dim1.high+1)(i)
	}

	test("row iterator") {
	  var i = a.index.dim1.low
	  for (row <- sa) { assertResult(a(i))(row); i += 1 }
    assertResult(a.index.dim1.high+1)(i)
	}

	test("column iterator") {
	  var j = a.index.dim2.low
	  for (col <- sa.colIterator) { assertResult(a.col(j))(col); j += 1 }
	}

	test("elem iterator") {
	  val elems = a.elemIterator.toSeq
	  var k = 0
		for (elem ← sa.elemIterator) { assertResult(elems(k))(elem); k += 1 }
	}

  test("to string") {
    assertResult("(-1:(-11,-12,-13)@1,1:(11,12,13)@1)")(sa.toString)
  }

  test("equal") {
	  val sa2 = Matrix(-1 → Vector(-11,-12,-13), +1 → Vector(11,12,13))
    assertResult(sa)(sa)
    assertResult(sa)(sa2)
    assert(a == sa)
    assert(sa == a)
    assert(sa != sb)
    assert(sa != b)
    assert(b != sa)
  }

	test("at") {
	  assertResult(a atRow 99)(sa atRow 99)
	  assertResult(a atCol 99)(sa atCol 99)
	  assertResult(a @@ (-99,99))(sa @@ (-99,99))
	  assertResult(1)(Matrix(1→Vector(111)).index.dim1.low)
	}

  test("shorten/widen") {
    assertResult(a.shorten)(sa.shorten)
	  val i = Index2(Index(-10,+10), Index(-5,+5))
	  assertResult(sa)(sa.widen(i).shorten)
	  assertResult(a.widen(i).shorten)(sa.widen(i).shorten)
	  assert(a.widen(i)~~sa.widen(i))
  }

  test("yield") {
    val ayr = for (row <- sa) yield row
	  assertResult(a)(ayr)
    val ac = for (col <- a.colIterator) yield col
    val ayc = for (col <- sa.colIterator) yield col
	  assertResult(Matrix(ac.toSeq:_*))(Matrix(ayc.toSeq:_*))
  }

	test("map") {
	  val am = a map { row => row*2 }
	  val sam = sa map { row => row*2 }
	  assertResult(am)(sam)
	}

  test("type") {
    assert((+sa).isInstanceOf[SparseMatrix[Int]])
    assert((-sa).isInstanceOf[SparseMatrix[Int]])
    assert((sa.transposed()).isInstanceOf[SparseMatrix[Int]])
    assert((sa+sb).isInstanceOf[SparseMatrix[Int]])
    assert((sa+b).isInstanceOf[SparseMatrix[Int]])
    assert((b+sa).isInstanceOf[Matrix[Int]])
    assert((sa-sb).isInstanceOf[SparseMatrix[Int]])
    assert((sa-b).isInstanceOf[SparseMatrix[Int]])
    assert((b-sa).isInstanceOf[Matrix[Int]])
    assert((2*sa).isInstanceOf[SparseMatrix[Int]])
    assert((sa*2).isInstanceOf[SparseMatrix[Int]])
    assert((sa*sb).isInstanceOf[SparseMatrix[Int]])
    assert((sa*b).isInstanceOf[SparseMatrix[Int]])
    assert((b*sa).isInstanceOf[Matrix[Int]])
    val v = Vector(1,2,3)
    assert((sa*v).isInstanceOf[Vector[Int]])
    assert((v**sa).isInstanceOf[Vector[Int]])
    val sv = Vector(1→1,2→2,3→3)
    assert((sa*sv).isInstanceOf[SparseVector[Int]])
    assert((sv**sa).isInstanceOf[Vector[Int]]) //should be sparse
    assert((a.map { row => row*2 }).isInstanceOf[Matrix[Int]]) //should be sparse
  }

}
