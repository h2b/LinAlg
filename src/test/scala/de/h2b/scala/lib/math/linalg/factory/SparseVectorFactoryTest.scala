/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.linalg.factory

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import de.h2b.scala.lib.math.linalg.Vector
import de.h2b.scala.lib.math.linalg.Vector.At

@RunWith(classOf[JUnitRunner])
class SparseVectorFactoryTest extends FunSuite {

  private val v = Vector.sparse(-10 → -1, 0 → 0, +10 → +1)

  test("sparse creation") {
	  assertResult(-1)(v(-10))
	  assertResult(0)(v(0))
	  assertResult(1)(v(10))
	  for (i ← 1 to 9) {
	    assertResult(0)(v(i))
	    assertResult(0)(v(-i))
   }
  }

  test("sparse at has no effect") {
    implicit val at = At(0)
    val w = Vector.sparse(-10 → -1, 0 → 0, +10 → +1)
    assertResult(v)(w)
  }

  test("sparse index range") {
    assertResult(-10)(v.index.low)
    assertResult(+10)(v.index.high)
  }

  test("sparse element type") {
	  val charVec = Vector.sparse(1 → 'a', 2 → 'b')
    assert(charVec(2).isInstanceOf[Char])
    val byteVec = Vector.sparse(1 → 1.toByte, 2 → 2.toByte)
    assert(byteVec(2).isInstanceOf[Byte])
    val shortVec = Vector.sparse(1 → 1.toShort, 2 → 2.toShort)
    assert(shortVec(2).isInstanceOf[Short])
    val intVec = Vector.sparse(1 → 1, 2 → 2)
    assert(intVec(2).isInstanceOf[Int])
    val longVec = Vector.sparse(1 → 1L, 2 → 2L)
    assert(longVec(2).isInstanceOf[Long])
    val floatVec = Vector.sparse(1 → 1f, 2 → 2f)
    assert(floatVec(2).isInstanceOf[Float])
    val doubleVec = Vector.sparse(1 → 1.0, 2 → 2.0)
    assert(doubleVec(2).isInstanceOf[Double])
  }

  test("sparse exception for unsupported type") {
    intercept[UnsupportedOperationException] {
      val stringVec = Vector.sparse(1 → "s1", 2 → "s2")
    }
  }

}
