/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/
package de.h2b.scala.lib.math.linalg

import scala.math.sqrt

import org.junit.runner.RunWith
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import de.h2b.scala.lib.math.{ DoubleApproxEqual, doubleTolerance }

@RunWith(classOf[JUnitRunner])
class SparseVectorTest extends FunSuite {

  private val su = Vector(-3 → -3, 1 → 1, 3 → 3)
  private val sv = Vector(-2 → -2, 4 → 4)
  private val u = Vector(-3, 0, 0, 0, 1, 0, 3)@@(-3)
  private val v = Vector(-2, 0, 0, 0, 0, 0, 4)@@(-2)
  private val w = Vector(1,2,3)

  test("scaling") {
    assertResult(u*(-2))(-2*su)
  }

  test("unary operations") {
	  assertResult(u)(+su)
	  assertResult(v)(+sv)
	  assertResult(-u)(-su)
	  assertResult(-v)(-sv)
  }

  test("binary plus operation") {
    assertResult(u+v)(su+sv)
    assertResult(u+w)(su+w)
    assertResult(u+w)(w+su)
  }

  test("binary minus operation") {
    assertResult(u-v)(su-sv)
    assertResult(w-u)(w-su)
    //While (u-w)(3) and (su-w)(3) both are equal to zero, the index 3 is
    //present in the concrete range of the first result, but not of the second;
    //thus, pure equality is not given, just similarity checked by ~~.
    val d1 = u-w
    val d2 = su-w
    assertResult(d1(3))(0)
    assertResult(d2(3))(0)
    assert(d1 ~~ d2)
  }

  test("scalar product") {
    assertResult(19)(su*su)
    assertResult(0)(su*sv)
    assertResult(100)(su*Vector(-1 → -1, 1 → 10, 3 → 30))
    assertResult(10)(su*w)
    assertResult(10)(w*su)
  }

  test("norm") {
    assert(sqrt(19) ~= su.norm)
  }

  test("iterator") {
    var i = u.index.low
    for (x <- su) { assertResult(u(i))(x); i += 1 }
    assertResult(u.index.high+1)(i)
  }

  test("sparse iterator") {
    val us = Vector(-3, 1, 3)
    var i = 1
    for (x <- su.sparseIterator) { assertResult(us(i))(x); i += 1 }
    assertResult(us.index.high+1)(i)
  }

  test("sparse index iterator") {
    assertResult(Seq(-3,1,3))(su.sparseIndexIterator.toSeq)
    assertResult(Seq(-2,4))(sv.sparseIndexIterator.toSeq)
  }

  test("sparse foreach") {
    val us = Vector(-3, 1, 3)
    var i = 1
    su.sparseForeach(x ⇒ { assertResult(us(i))(x); i +=1 })
    assertResult(us.index.high+1)(i)
  }

  test("to string") {
    assertResult("(-3:-3,1:1,3:3)")(su.toString)
  }

  test("equal") {
	  val su2 = Vector(-3 → -3, 1 → 1, 3 → 3)
    assertResult(su)(su)
    assertResult(su)(su2)
    assert(u == su)
    assert(su == u)
    assert(su != sv)
    assert(su != w)
    assert(w != su)
  }

  test("at") {
	  assertResult(u@@5)(su@@5)
	  assertResult(1)(Vector(1→111).index.low)
  }

  test("shorten/widen") {
    //see comment to binary minus operation
    assertResult(u.shorten)(su.shorten)
    val wu = u.widen(Index(-10, 10))
    val wsu = su.widen(Index(-10, 10))
    assert(wu ~~ wsu)
    assertResult(wu.shorten)(wsu.shorten)
    assertResult(su)(wsu.shorten)
  }

  test("yield") {
    val uy = for (x ← su) yield x
    assertResult(u)(uy)
  }

	test("map") {
	  val um = u map { x => x*2 }
	  val sum = su map { x => x*2 }
	  assertResult(um)(sum)
	}

  test("type") {
    assert((+su).isInstanceOf[SparseVector[Int]])
    assert((-su).isInstanceOf[SparseVector[Int]])
    assert((su+sv).isInstanceOf[SparseVector[Int]])
    assert((su+w).isInstanceOf[SparseVector[Int]])
    assert((w+su).isInstanceOf[Vector[Int]])
    assert((su-sv).isInstanceOf[SparseVector[Int]])
    assert((su-w).isInstanceOf[SparseVector[Int]])
    assert((w-su).isInstanceOf[Vector[Int]])
    assert((2*su).isInstanceOf[SparseVector[Int]])
    assert((su*2).isInstanceOf[SparseVector[Int]])
  }

}
