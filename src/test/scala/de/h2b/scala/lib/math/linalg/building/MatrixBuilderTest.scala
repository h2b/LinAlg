/*
  LinAlg - Scala Library for Vector and Matrix Types and Operations

  Copyright 2015-2018 Hans-Hermann Bode

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package de.h2b.scala.lib.math.linalg.building

import org.junit.runner.RunWith
import org.scalactic.TolerantNumerics
import org.scalatest.FunSuite
import org.scalatest.junit.JUnitRunner

import de.h2b.scala.lib.math.linalg.{ Matrix, Vector }

@RunWith(classOf[JUnitRunner])
class MatrixBuilderTest extends FunSuite {

	implicit val doubleEquality = TolerantNumerics.tolerantDoubleEquality(1e-6)

  private def check (expected: Array[Array[Double]], actual: Matrix[Double], rowRange: Range, colRange: Range) = {
	  assert(rowRange.size == actual.height)
	  assert(colRange.size == actual.width)
	  for {
	    i <- rowRange
	    j <- colRange
	  } assert(expected(i-rowRange.start)(j-colRange.start) === actual(i,j))
  }

	test("Building a matrix at default start indices is OK") {
	  val v1 = Vector(1.0,2.0,3.0)
	  val v2 = Vector(4.0,5.0,6.0)
	  val builder = MatrixBuilder[Double]()
	  builder += v1
	  builder += v2
	  val a = builder.result()
	  assert(a.isInstanceOf[Matrix[Double]])
	  check(Array(Array(1.0,2.0,3.0),Array(4.0,5.0,6.0)), a, 1 to 2, 1 to 3)
	}

	test("Building a matrix at explicit row start is OK") {
	  val v1 = Vector(1.0,2.0,3.0)
	  val v2 = Vector(4.0,5.0,6.0)
	  val builder = MatrixBuilder[Double]()
	  builder += v1
	  builder += v2
	  builder at 10
	  val a = builder.result()
	  assert(a.isInstanceOf[Matrix[Double]])
	  check(Array(Array(1.0,2.0,3.0),Array(4.0,5.0,6.0)), a, 10 to 11, 1 to 3)
	}

	test("Building a matrix using updates is OK") {
	  val v1 = Vector(1.0,2.0,3.0)
	  val v2 = Vector(4.0,5.0,6.0)
	  val builder = MatrixBuilder[Double]()
	  builder(+1) = v2
	  builder(-1) = v1
	  val a = builder.result()
	  assert(a.isInstanceOf[Matrix[Double]])
	  check(Array(Array(1.0,2.0,3.0),Array(0.0, 0.0, 0.0),Array(4.0,5.0,6.0)), a, -1 to 1, 1 to 3)
	}

}