LinAlg -- Performance checks

Date: Fri May 13 23:10:10 CEST 2016

N = 1000
M = 100
Repeats = 500
             

                task 	         time/ns

       matrixColSum$ 	     9262757,912
       matrixCreate$ 	     5133720,360
        matrixMinus$ 	    11026189,316
         matrixPlus$ 	     4040686,186
       matrixRowSum$ 	     2078351,400
  matrixTimesMatrix$ 	   888925706,830
  matrixTimesScalar$ 	     4105604,458
  matrixTimesVector$ 	     2048209,140
   matrixTransposed$ 	     6759412,694
   matrixUnaryMinus$ 	     4037316,336
    matrixUnaryPlus$ 	     3958981,890
       vectorCreate$ 	      149311,196
        vectorMinus$ 	       71845,322
         vectorNorm$ 	       29102,882
         vectorPlus$ 	      123966,408
vectorScalarProduct$ 	       82195,598
  vectorTimesScalar$ 	       62556,108
   vectorUnaryMinus$ 	       73286,130
    vectorUnaryPlus$ 	      113604,530

                 sum 	    471041402348 (7m51s41ms)
