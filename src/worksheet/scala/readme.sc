object readme {

  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  import de.h2b.scala.lib.math.linalg._
  
  val vi = Vector(1,2,3)                          //> vi  : de.h2b.scala.lib.math.linalg.Vector[Int] = (1,2,3)@1
  val vd = Vector(1.0,2.0,3.0)                    //> vd  : de.h2b.scala.lib.math.linalg.Vector[Double] = (1.0,2.0,3.0)@1
  
  val vat0 = Vector.at(0)(1,2,3)                  //> vat0  : de.h2b.scala.lib.math.linalg.Vector[Int] = (1,2,3)@0
  
  val vf = Vector((i: Int) => i*i: Double, 1, 3)  //> vf  : de.h2b.scala.lib.math.linalg.Vector[Double] = (1.0,4.0,9.0)@1
  
	val vbuilder1 = Vector.newBuilder[Double] //> vbuilder1  : de.h2b.scala.lib.math.linalg.VectorBuilder[Double,de.h2b.scala.
                                                  //| lib.math.linalg.Vector[Double]] = de.h2b.scala.lib.math.linalg.DefaultVector
                                                  //| Builder@3a03464
	vbuilder1 += 1.0                          //> res0: readme.vbuilder1.type = de.h2b.scala.lib.math.linalg.DefaultVectorBuil
                                                  //| der@3a03464
	vbuilder1 += 2.0                          //> res1: readme.vbuilder1.type = de.h2b.scala.lib.math.linalg.DefaultVectorBuil
                                                  //| der@3a03464
	vbuilder1 += 3.0                          //> res2: readme.vbuilder1.type = de.h2b.scala.lib.math.linalg.DefaultVectorBuil
                                                  //| der@3a03464
	val vb1 = vbuilder1.result()              //> vb1  : de.h2b.scala.lib.math.linalg.Vector[Double] = (1.0,2.0,3.0)@1

  val vbuilder2 = Vector.newBuilder[Double]       //> vbuilder2  : de.h2b.scala.lib.math.linalg.VectorBuilder[Double,de.h2b.scala.
                                                  //| lib.math.linalg.Vector[Double]] = de.h2b.scala.lib.math.linalg.DefaultVector
                                                  //| Builder@2d3fcdbd
	vbuilder2(1) = 1.0
	vbuilder2(3) = 3.0
	val vb2 = vbuilder2.result()              //> vb2  : de.h2b.scala.lib.math.linalg.Vector[Double] = (1.0,0.0,3.0)@1

	val a = Matrix(Vector(11,12,13), Vector(21,22,23))
                                                  //> a  : de.h2b.scala.lib.math.linalg.Matrix[Int] = ((11,12,13)@1,(21,22,23)@1)@
                                                  //| 1,1
	val b = Matrix(Vector.at(0)(10,11,12), Vector.at(0)(20,21,22))
                                                  //> b  : de.h2b.scala.lib.math.linalg.Matrix[Int] = ((10,11,12)@0,(20,21,22)@0)@
                                                  //| 1,0
  val c = Matrix.atRow(0)(Vector(1,2,3), Vector(11,12,13))
                                                  //> c  : de.h2b.scala.lib.math.linalg.Matrix[Int] = ((1,2,3)@1,(11,12,13)@1)@0,1
                                                  //| 
                                                  
	val af = Matrix((i: Int) => Vector(i*10+1,i*10+2,i*10+3), 1, 2)
                                                  //> af  : de.h2b.scala.lib.math.linalg.Matrix[Int] = ((11,12,13)@1,(21,22,23)@1)
                                                  //| @1,1
	
	val row1 = Vector(1,2,3)                  //> row1  : de.h2b.scala.lib.math.linalg.Vector[Int] = (1,2,3)@1
	val row2 = Vector(4,5,6)                  //> row2  : de.h2b.scala.lib.math.linalg.Vector[Int] = (4,5,6)@1
	val mbuilder1 = Matrix.newBuilder[Int]    //> mbuilder1  : de.h2b.scala.lib.math.linalg.MatrixBuilder[de.h2b.scala.lib.mat
                                                  //| h.linalg.Vector[Int],de.h2b.scala.lib.math.linalg.Matrix[Int]] = de.h2b.scal
                                                  //| a.lib.math.linalg.DefaultMatrixBuilder@2669b199
	mbuilder1 += row1                         //> res3: readme.mbuilder1.type = de.h2b.scala.lib.math.linalg.DefaultMatrixBuil
                                                  //| der@2669b199
	mbuilder1 += row2                         //> res4: readme.mbuilder1.type = de.h2b.scala.lib.math.linalg.DefaultMatrixBuil
                                                  //| der@2669b199
	val ab1 = mbuilder1.result()              //> ab1  : de.h2b.scala.lib.math.linalg.Matrix[Int] = ((1,2,3)@1,(4,5,6)@1)@1,1
	
	val mbuilder2 = Matrix.newBuilder[Int]    //> mbuilder2  : de.h2b.scala.lib.math.linalg.MatrixBuilder[de.h2b.scala.lib.mat
                                                  //| h.linalg.Vector[Int],de.h2b.scala.lib.math.linalg.Matrix[Int]] = de.h2b.scal
                                                  //| a.lib.math.linalg.DefaultMatrixBuilder@2344fc66
	mbuilder2(1) = row1
	mbuilder2(3) = row2
	val ab2 = mbuilder2.result()              //> ab2  : de.h2b.scala.lib.math.linalg.Matrix[Int] = ((1,2,3)@1,()@1,(4,5,6)@1
                                                  //| )@1,1
	
  val v0 = Vector[Double]()                       //> v0  : de.h2b.scala.lib.math.linalg.Vector[Double] = ()@1
	val a0 = Matrix[Double]()                 //> a0  : de.h2b.scala.lib.math.linalg.Matrix[Double] = ()@1,2147483646
	
  Vector(1,2) == Vector(1.0,2.0)                  //> res5: Boolean = true
  Vector(1,2) != Vector(1,2,0)                    //> res6: Boolean = true
  Vector(1,2) != Vector(1,3)                      //> res7: Boolean = true
  Vector(1,2) == Vector(1,2)                      //> res8: Boolean = true
  Vector(1,2) ~~ Vector(1,2,0)                    //> res9: Boolean = true
  ! (Vector(1,2) ~~ Vector(1,3))                  //> res10: Boolean = true
  Vector(1,2) ~~ Vector(1,2)                      //> res11: Boolean = true
  
}